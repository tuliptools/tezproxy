package tezclient

import (
	"encoding/json"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/tuliptools/tezproxy/config"
	"gitlab.com/tuliptools/tezproxy/tezmodels"
	"io/ioutil"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

type TezNode struct {
	Addr     string
	Head     uint64
	Up       bool
	HeadHash string
}

type TezClient struct {
	nodes      map[string][]*TezNode
	nodesLock  *sync.Mutex
	conf       *config.Config
	fastHttp   *http.Client
	normalHttp *http.Client
	log        *logrus.Logger

	headsLock *sync.Mutex
}

func NewTezClient(conf *config.Config, log *logrus.Logger) *TezClient {
	this := TezClient{}
	this.conf = conf
	this.nodes = map[string][]*TezNode{}
	this.nodesLock = &sync.Mutex{}
	this.headsLock = &sync.Mutex{}
	this.log = log

	for _, n := range this.conf.TezosNodes {
		this.nodes[n.Network] = []*TezNode{}
	}

	for _, n := range this.conf.TezosNodes {
		this.nodes[n.Network] = append(this.nodes[n.Network], &TezNode{
			Addr: n.Addr,
			Up:   false,
			Head: 0,
		})
	}

	timeout := time.Duration(5 * time.Second)
	client := http.Client{
		Timeout: timeout,
	}

	this.fastHttp = &client

	timeout2 := time.Duration(10 * time.Second)
	client2 := http.Client{
		Timeout: timeout2,
	}

	this.normalHttp = &client2

	go this.checkNodes()

	return &this
}

func (this *TezClient) getAddr(network string) (string, error) {
	upnodes := []*TezNode{}
	syncedNodes := []*TezNode{}
	highestLevel := uint64(0)
	for _, n := range this.nodes[network] {
		if n.Up == true {
			upnodes = append(upnodes, n)
			if n.Head > highestLevel {
				highestLevel = n.Head
			}
		}
	}

	for _, n := range upnodes {
		if n.Head >= highestLevel {
			syncedNodes = append(syncedNodes, n)
		}
	}

	if len(syncedNodes) == 0 {
		return "", errors.New("No available nodes for network " + network)
	} else {
		id := rand.Int() % len(syncedNodes)
		return syncedNodes[id].Addr, nil
	}

}

func (this *TezClient) GetHeadHash(network string) (string, error) {
	upnodes := []*TezNode{}
	syncedNodes := []*TezNode{}
	highestLevel := uint64(0)
	for _, n := range this.nodes[network] {
		if n.Up == true {
			upnodes = append(upnodes, n)
			if n.Head > highestLevel {
				highestLevel = n.Head
			}
		}
	}

	for _, n := range upnodes {
		if n.Head >= highestLevel {
			syncedNodes = append(syncedNodes, n)
		}
	}

	if len(syncedNodes) == 0 {
		return "", errors.New("No available nodes for network " + network)
	} else {
		id := rand.Int() % len(syncedNodes)
		return syncedNodes[id].HeadHash, nil
	}

}

func (this *TezClient) Get(network, url, body string) ([]byte, error) {
	node, err := this.getAddr(network)
	if err != nil {
		return nil, err
	}

	requestUrl := node + url
	resp, err := this.normalHttp.Get(requestUrl)
	if err != nil {
		return nil, err
	}
	b, _ := ioutil.ReadAll(resp.Body)
	return b, nil
}

func (this *TezClient) GetURL(network string) (string, error) {
	node, err := this.getAddr(network)
	if err != nil {
		return "", err
	}
	requestUrl := node
	return requestUrl, nil
}

func (this *TezClient) checkNodes() {
	for {
		for network, nodes := range this.nodes {
			for _, node := range nodes {
				res, err := this.getBlock(node.Addr, "head")
				if err == nil && res.Header.Level >= 1 {
					if node.Up == false {
						this.log.Info("Tezos Node ", node.Addr, " for Network ", network, " changed state to UP")
					}
					node.Up = true
					node.Head = res.Header.Level
					node.HeadHash = res.Hash
				} else {
					this.log.Warn("Error checking node ", node.Addr, " :: ", err)
					if node.Up {
						this.log.Info("Tezos Node ", node.Addr, " for Network ", network, " changed state to DOWN")
					}
					node.Up = false
				}
			}
		}
		time.Sleep(25 * time.Second)
	}
}

func (this *TezClient) getBlock(addr, hash string) (*tezmodels.Block, error) {
	res := tezmodels.Block{}
	url := addr + "/chains/main/blocks/" + hash
	resp, err := this.fastHttp.Get(url)
	if err != nil {
		return &res, err
	}
	body, _ := ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &res)
	return &res, nil
}
