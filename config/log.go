package config

import (
	log "github.com/sirupsen/logrus"
)

func GetLog(c *Config) *log.Logger {

	logger := log.New()

	if c.LogType == "json" {
		logger.SetFormatter(&log.JSONFormatter{})
	}

	if c.LogType == "text" {
		logger.SetFormatter(&log.TextFormatter{})
	}

	return logger

}
