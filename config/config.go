package config

import (
	"fmt"
	"github.com/spf13/viper"
)

type Config struct {
	TezosNodes    []TezosNodes
	LogType       string
	CacheSize     int64
	ListenAddr    string
	AllowOrigins  []string
	AllowHeaders  []string
	AllowMethods  []string
	Whitelist     []string
	Blacklist     []string
	PeerNetwork   string
	SingleNetwork bool
}

type TezosNodes struct {
	Network string
	Addr    string
}

func GetConfig() *Config {
	c := Config{}

	viper.SetConfigName("config")
	viper.AddConfigPath("/etc/tezproxy/")
	viper.AddConfigPath("$HOME/.tezproxy")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			panic(err)
		} else {
			panic(err)
		}
	}

	err = viper.Unmarshal(&c)

	if err != nil {
		panic(err)
	}

	return &c
}
