FROM golang:latest
WORKDIR /app
COPY . .
RUN go build -o main .

FROM golang:latest
COPY --from=0 /app/main ./main
CMD ["./main"]