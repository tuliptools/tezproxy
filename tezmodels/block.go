package tezmodels

type Block struct {
	Hash   string `json:"hash"`
	Header struct {
		Level uint64 `json:"level"`
	} `json:"header"`
}
