package proxy

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/tuliptools/tezproxy/cache"
	"gitlab.com/tuliptools/tezproxy/config"
	"gitlab.com/tuliptools/tezproxy/tezclient"
	"math"
	"net/http"
	"net/http/httputil"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type Proxy struct {
	config       *config.Config
	networks     map[string]bool
	log          *logrus.Logger
	cache        *cache.GCWrapper
	tez          *tezclient.TezClient
	http         *http.Client
	whitelistedR []*regexp.Regexp
	blacklsitedR []*regexp.Regexp
}

func NewProxy(c *config.Config, l *logrus.Logger, cache *cache.GCWrapper, tez *tezclient.TezClient) *Proxy {
	t := Proxy{}
	t.config = c
	t.log = l
	t.cache = cache
	t.tez = tez
	t.networks = map[string]bool{}
	for _, n := range c.TezosNodes {
		t.networks[n.Network] = true
	}

	timeout := 10 * time.Second
	client := http.Client{
		Timeout: timeout,
	}

	t.http = &client

	t.setupRegexp()

	return &t
}

func (this *Proxy) rewriteGetUrl(network, url string) string {
	head, _ := this.tez.GetHeadHash(network)
	url = strings.Replace(url, "header", "XHEADERX", 1)
	url = strings.Replace(url, "/head", "/"+head, 1)
	url = strings.Replace(url, "XHEADERX", "header", 1)
	return url
}

func (this *Proxy) setupRegexp() {
	for _, s := range this.config.Blacklist {
		regex, err := regexp.Compile(s)
		if err != nil {
			this.log.Error("Cant compile Regexp: ", s)
		} else {
			this.blacklsitedR = append(this.blacklsitedR, regex)
		}
	}
	for _, s := range this.config.Whitelist {
		regex, err := regexp.Compile(s)
		if err != nil {
			this.log.Error("Cant compile Regexp: ", s)
		} else {
			this.whitelistedR = append(this.whitelistedR, regex)
		}
	}
}

func (this *Proxy) isAllowed(url string) bool {
	ret := false
	urls := strings.Split(url, "?")
	url = "/" + strings.Trim(urls[0], "/")
	for _, wl := range this.whitelistedR {
		if wl.Match([]byte(url)) {
			ret = true
			for _, bl := range this.blacklsitedR {
				if bl.Match([]byte(url)) {
					ret = false
					break
				}
			}
			break
		}
	}
	return ret
}

func (this *Proxy) getHandleFunc(c *gin.Context) {
	network := c.Param("network")
	this.genericGetHandleFunc(c, network)
}

func (this *Proxy) getDefaultHandleFunc(c *gin.Context) {
	this.genericGetHandleFunc(c, "remotenet")
}

func (this *Proxy) genericGetHandleFunc(c *gin.Context, network string) {
	rurl := c.Param("proxyPath")

	if strings.Contains(c.Request.RequestURI, "monitor") {
		geturl, e := this.tez.GetURL(network)
		if e != nil {
			c.String(500, "error getting GET url")
		}
		remote, err := url.Parse(geturl)
		if err != nil {
			panic(err)
		}

		if remote.Scheme == "https" {
			c.Request.URL.Host = remote.Host
			c.Request.URL.Scheme = remote.Scheme
			c.Request.Header.Set("X-Forwarded-Host", c.Request.Header.Get("Host"))
			c.Request.Host = remote.Host
		}
		c.Request.RequestURI = strings.Replace(c.Request.RequestURI, "/"+network, "", 1)
		c.Request.URL, _ = url.Parse(c.Request.RequestURI)
		proxy := httputil.NewSingleHostReverseProxy(remote)
		proxy.ServeHTTP(c.Writer, c.Request)
		return
	}

	c.Header("Content-Type", "application/json")

	if !this.isAllowed(rurl) {
		c.String(400, "{\"Error\":\"request not allowed\"}")
		return
	}

	if val, ok := this.networks[network]; !ok || val == false {
		c.String(404, "{\"Error\": \"network not found\"}")
		return
	}

	oldurl := rurl
	// rewrite /head to actual block, makes it easier for caching lib
	rurl = this.rewriteGetUrl(network, rurl)
	if c.Request.URL.Query().Encode() != "" {
		rurl = rurl + "?" + c.Request.URL.Query().Encode()
	}

	val, err := this.cache.Get(network + ":::::" + rurl)

	if err != nil {
		c.Error(errors.New("internal server Error"))
	} else {

		// cdn caching https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.9.3
		c.Header("Cache-Control", "public")

		if strings.HasSuffix(oldurl, "/head") || strings.HasSuffix(oldurl, "/head/") {
			// set headers for CDN caching
			// cache until next full minute because tez blocks happen in that interval
			toRound := time.Now()
			rounded := time.Date(toRound.Year(), toRound.Month(), toRound.Day(), toRound.Hour(), toRound.Minute()+1, 0, 0, toRound.Location())
			cacheSec := int(math.Floor(rounded.Sub(toRound).Seconds()))
			c.Header("Cache-Control", "max-age="+strconv.Itoa(cacheSec))
		} else { // if not -> specific block -> cache for longer
			c.Header("Cache-Control", "max-age=3600")

		}

		c.String(200, string(val))
	}
}

func (this *Proxy) postHandlerFunc(c *gin.Context) {
	network := c.Param("network")
	this.genericPostHandleFunc(c, network)
}

func (this *Proxy) postDefaultHandlerFunc(c *gin.Context) {
	this.genericPostHandleFunc(c, "remotenet")
}

func (this *Proxy) genericPostHandleFunc(c *gin.Context, network string) {
	rurl := c.Param("url")
	c.Header("Content-Type", "application/json")
	if val, ok := this.networks[network]; !ok || val == false {
		c.String(404, "network not found!")
		return
	}

	if !this.isAllowed(rurl) {
		c.String(500, "request not allowed")
		return
	}

	posturl, e := this.tez.GetURL(network)
	if e != nil {
		c.String(500, "error getting POST url")
	}

	remote, err := url.Parse(posturl)
	if err != nil {
		panic(err)
	}
	if remote.Scheme == "https" {
		c.Request.URL.Host = remote.Host
		c.Request.URL.Scheme = remote.Scheme
		c.Request.Header.Set("X-Forwarded-Host", c.Request.Header.Get("Host"))
		c.Request.Host = remote.Host
	}
	c.Request.RequestURI = strings.Replace(c.Request.RequestURI, "/"+network, "", 1)
	c.Request.URL, _ = url.Parse(c.Request.RequestURI)
	proxy := httputil.NewSingleHostReverseProxy(remote)
	proxy.ServeHTTP(c.Writer, c.Request)
}

func (this *Proxy) Run(singlenet bool) {
	r := gin.Default()
	r.Use(cors.New(cors.Config{
		AllowOrigins:     this.config.AllowOrigins,
		AllowMethods:     this.config.AllowMethods,
		AllowHeaders:     this.config.AllowHeaders,
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	if !singlenet {
		r.GET("/:network/*proxyPath", this.getHandleFunc)
		r.POST("/:network/*proxyPath", this.postHandlerFunc)
	} else {
		r.GET("/*proxyPath", this.getDefaultHandleFunc)
		r.POST("/*proxyPath", this.postDefaultHandlerFunc)
	}

	s := &http.Server{
		Addr:           this.config.ListenAddr,
		Handler:        r,
		ReadTimeout:    1 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
		IdleTimeout:    1 * time.Second,
	}
	e := s.ListenAndServe()
	if e != nil {
		panic(e)
	}

}
