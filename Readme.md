# Tezos Proxy

This Is a relatively simple Proxy for the Tezos RPC API that does caching and improves the Performance beyond the 
normal Tezos RPC API, you can test it here:

https://rpc.tulip.tools/

(dashboard not included)

It can White- and Blacklist URLs so that you can safely run a public Teozs RPC API
and tries to keep as many responses as possible in a LRU cache ( last-recently-used ), if you run multiple instances
of this Proxy on the same network, it will detect copies of its own and make sure each cache entry only exists once on your
local Network (nodes need to be configured with different hostnames for this).

## Usage

```
$ tezroxy run
```



## API

Normal RPC Queries can be made via 

```
tezproxy:port/<networkname>/..
```

so for Example:


```
tezproxy:port/mainnet/chains/main/blocks/head/...
```


## Config

### Tezos Nodes

You can specify one or multiple Tezos nodes running one or multiple Networks, if you run a custom tezos network for testing or development,
you can invent a network name and it will load balance among nodes with the same network-name

For Example:

```
TezosNodes:
  - Network: "mainnet"
    Addr: "172.20.0.2:8732"
  - Network: "mainnet"
    Addr: "172.20.0.3:8732"
  - Network: "alphanet"
    Addr: "172.22.0.2:8732"
  - Network: "mytestchain"
    Addr: "172.23.0.2:8732"
  - Network: "mytestchain"
    Addr: "172.23.0.3:8732"        
```

### Log Output

2 Types of Log Output: `json` and `text`

```
LogType: text
```

### Rate Limit

You can enable rate-limiting and specify Limit and Burst rates, highly recommended for public Proxies ( like https://rpc.tulip.tools )
If enabled, it will allow  events up to rate `Rate` per Minute. Rate Limits are kept per IP Address
( it will automatically detect if it is running behind another Proxy like Cloudflare, and check Request Headers to get the source IP e.g. X-Forwarded-For etc).

```
RateLimit:
  Enable: true
  Rate: 80
```


### Cache

Specifies maximum cache size in MB, 0 means cache disabled and tezproxy will perform only load balancing, not recommended.
This is RAM only, tezproxy does not store any data on disk.


```
CacheSize: 200
```


### Listen Addr

define where the proxy wil listen

```
ListenAddr: ":80"   # Listen to all Ips on port 80
#ListenAddr: "127.0.0.1:80"     # Listen to only Localhost on port 80
```


### Cors Configuration

Depending on wheter or not you run this as a public service, CORS config is recommended

```
AllowOrigins: ["*"]
AllowMethods: ["GET","POST","OPTIONS"]
AllowHeaders: ["*"]
```

### Peer Discovery

If you Run multiple Instances of this on the same network, they will discover each other via udp multicast, so you dont
need to specify its peers, however you do need to specify the network its peers are on in CIDR notation

```
PeerNetwork: "172.22.0.0/16"
```

Make sure your network supports udp mutlicast, the standard docker swarm overlay network type does not for example,
we run this in production on docker swarm and we use Weave Net as net plugin for example.


In addition to udp the instances will coordinate themselves via tcp/9092 so make sure they can reach each other.


### config.yml

Tezproxy looks for a file called `config.yml` ( or .json) in:
 
 * /etc/tezproxy/
 * $HOME/.tezproxy/
 * ./
 

```

TezosNodes:
  - Network: "mainnet"
    Addr: "172.10.0.2:8732"

  - Network: "mainnet"
    Addr: "172.10.0.3:8732"

  - Network: "mainnet"
    Addr: "172.10.0.4:8732"

  - Network: "mainnet"
    Addr: "172.10.0.6:8732"

  - Network: "alphanet"
    Addr: "172.10.0.8:8732"

  - Network: "zeronet"
    Addr: "172.10.0.10:8732"

  - Network: "sandboxnet"
    Addr: "172.10.0.12:8732"


LogType: text # or json

RateLimit:
  Enable: true
  Rate: 80 # per minute per IP


# max-size in MB
CacheSize: 200

# CORS
AllowOrigins: ["*"]
AllowMethods: ["GET","POST","OPTIONS"]
AllowHeaders: ["*"]


# API Port
ListenAddr: "0.0.0.0:8080"

# Peer discovery
PeerNetwork: "172.22.0.0/16"


# recomended settings for public proxies
# leave empty for private setups
Whitelist:
  - "/chains/main/blocks(.*?)"
Blacklist:
  - "(.*?)context/contracts$"
  - "/monitor(.*?)"
  - "/network(.*?)"
  ```