package cache

import (
	"bytes"
	"github.com/golang/groupcache"
	"github.com/schollz/peerdiscovery"
	"github.com/sirupsen/logrus"
	"gitlab.com/tuliptools/tezproxy/config"
	"gitlab.com/tuliptools/tezproxy/tezclient"
	math_rand "math/rand"
	net2 "net"
	"net/http"
	"strings"
	"sync"
	"time"
)

type GCWrapper struct {
	config      *config.Config
	Group       *groupcache.Group
	tez         *tezclient.TezClient
	log         *logrus.Logger
	peerPayload []byte
}

func NewGroupCacheWrapper(c *config.Config, tez *tezclient.TezClient, log *logrus.Logger) *GCWrapper {
	res := GCWrapper{}
	res.config = c
	res.tez = tez
	res.log = log
	res.peerPayload = []byte(randStringBytesMaskImprSrc(10))

	group := groupcache.NewGroup("tezrpc", c.CacheSize<<20, groupcache.GetterFunc(
		func(ctx groupcache.Context, key string, dest groupcache.Sink) error {
			splits := strings.Split(key, ":::::")
			network := splits[0]
			url := "/" + splits[1]
			res.log.Info("Lookup: ", network, url)
			res, e := res.tez.Get(network, url, "")
			if e != nil {
				return e
			}
			return dest.SetBytes(res)
		},
	))

	res.Group = group

	go res.lookForPeers()
	return &res
}

func (this *GCWrapper) Get(key string) ([]byte, error) {
	var res []byte
	err := this.Group.Get(nil, key, groupcache.AllocatingByteSliceSink(&res))
	return res, err
}

func (this *GCWrapper) lookForPeers() {
	meset := false
	me := ""
	var peers *groupcache.HTTPPool
	setlock := &sync.Mutex{}
	peersList := map[string]time.Time{}

	peerdiscovery.Discover(peerdiscovery.Settings{
		Limit:     -1,
		Payload:   this.peerPayload,
		Delay:     1000 * time.Millisecond,
		TimeLimit: -10 * time.Second,
		AllowSelf: true,
		Notify: func(d peerdiscovery.Discovered) {
			setlock.Lock()
			defer setlock.Unlock()
			ip, ipnet, _ := net2.ParseCIDR(this.config.PeerNetwork)
			addr := net2.ParseIP(d.Address)

			if ipnet.Contains(addr) && bytes.Compare(d.Payload, this.peerPayload) == 0 && meset == false {
				peers = groupcache.NewHTTPPool("http://" + d.Address + ":9092")
				meset = true
				me = d.Address
				go func() {
					http.ListenAndServe("0.0.0.0:9092", http.HandlerFunc(peers.ServeHTTP))
				}()

			}

			if ipnet.Contains(ip) && bytes.Compare(d.Payload, this.peerPayload) != 0 && meset {
				peersList["http://"+d.Address+":9092"] = time.Now()
				peersList["http://"+me+":9092"] = time.Now()
				list := []string{}
				for ip, t := range peersList {
					if time.Now().Add(-10 * time.Second).After(t) {
						delete(peersList, ip)
					} else {
						list = append(list, ip)
					}
				}
				peers.Set(list...)
			}
		},
	})

}

// Helper Funcs

var randSrc = math_rand.NewSource(time.Now().UnixNano())

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

// RandStringBytesMaskImprSrc prints a random string
func randStringBytesMaskImprSrc(n int) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, randSrc.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = randSrc.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}
