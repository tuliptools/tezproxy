module gitlab.com/tuliptools/tezproxy

go 1.13

require (
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/gin-gonic/gin v1.4.0
	github.com/golang/groupcache v0.0.0-20190702054246-869f871628b6
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/hashicorp/golang-lru v0.5.3 // indirect
	github.com/json-iterator/go v1.1.7 // indirect
	github.com/karlseguin/ccache v2.0.3+incompatible
	github.com/mattn/go-isatty v0.0.8 // indirect
	github.com/pkg/errors v0.8.0
	github.com/schollz/peerdiscovery v1.4.1
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/viper v1.4.0
	github.com/toorop/gin-logrus v0.0.0-20190701131413-6c374ad36b67
	github.com/ugorji/go v1.1.7 // indirect
	go.uber.org/dig v1.7.0
	golang.org/x/sys v0.0.0-20190804053845-51ab0e2deafa // indirect
)
