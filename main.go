package main

import (
	"gitlab.com/tuliptools/tezproxy/cache"
	"gitlab.com/tuliptools/tezproxy/config"
	"gitlab.com/tuliptools/tezproxy/proxy"
	"gitlab.com/tuliptools/tezproxy/tezclient"
	"gitlab.com/tuliptools/tezproxy/util"
	"go.uber.org/dig"
	"math/rand"
	"os"
	"os/signal"
	"time"
)

func main() {

	hostname, _ := os.Hostname()

	rand.Seed(time.Now().UTC().UnixNano() + int64(util.HashToNum(hostname)%65000))

	container := dig.New()
	container.Provide(config.GetConfig)
	container.Provide(config.GetLog)
	container.Provide(proxy.NewProxy)
	container.Provide(tezclient.NewTezClient)
	container.Provide(cache.NewGroupCacheWrapper)

	container.Invoke(func(p *proxy.Proxy, c *config.Config) {
		p.Run(c.SingleNetwork)
	})

	var signal_channel chan os.Signal
	signal_channel = make(chan os.Signal, 1)
	signal.Notify(signal_channel, os.Interrupt)
	<-signal_channel

}
