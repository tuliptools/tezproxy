package util

import (
	"hash/fnv"
)

func HashToNum(s string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	return h.Sum32()
}
